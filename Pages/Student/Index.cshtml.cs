﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Pages.Student
{
    public class IndexModel : PageModel
    {
        private readonly RazorPages.Models.RazorPagesContext _context;

        public IndexModel(RazorPages.Models.RazorPagesContext context)
        {
            _context = context;
        }

        public IList<Models.Student> Student { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        // Requires using Microsoft.AspNetCore.Mvc.Rendering;
        public SelectList FirstNames { get; set; }
        [BindProperty(SupportsGet = true)]
        public string StudentsFirstName { get; set; }

        public async Task OnGetAsync()
        {
            var students = from m in _context.Student
                         select m;
            if (!string.IsNullOrEmpty(SearchString))
            {
                students = students.Where(s => s.FirstName.Contains(SearchString));
            }

            Student = await students.ToListAsync();
        }

        public async Task OnPostGreaterThanAsync(int gpa)
        {
            var students = from m in _context.Student
                           select m;
            
         students = students.Where(s => s.GPA>gpa);
        
            Student = await students.ToListAsync();
        }
        public async Task OnPostLessThanAsync(int gpa)
        {
            var students = from m in _context.Student
                           select m;

            students = students.Where(s => s.GPA < gpa);

            Student = await students.ToListAsync();
        }
    }
}
